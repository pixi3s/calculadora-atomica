'''
    Funções Matematicas para nossa calculadora:
    Soma +
    Subtração -
    Multiplicação *
    Divisão /
'''

#Recebe dois valores e retorna a soma desses valores
def soma(val1, val2):
    return val1+val2

#Recebe dois valores e retorna a subtração desses valores
def subtracao(val1, val2):
    return val1-val2

#Recebe dois valores e retorna a multiplicação desses valores
def multiplicacao(val1, val2):
    return val1*val2

#Recebe dois valores e retorna a divisão desses valores
def divisao(val1, val2):
    if val2 == 0:
        return 'Não pode ser divido por zero'
    return val1/val2
