from funcoes_matematica import (
                                soma, 
                                subtracao, 
                                multiplicacao, 
                                divisao
                                )


print('=' *40)
print('Bem vindo a calculadora atômica')

val1 = float(input('Digite valor 1:\n'))
operacao = input('Informe a operação que você deseja:\n')
val2 = float(input('Digite valor 2:\n'))

if operacao == '+':
    print(soma(val1,val2))
elif operacao == '-':
    print(subtracao(val1, val2))
elif operacao == '*':
    print(multiplicacao(val1, val2))
elif operacao == '/':
    print(divisao(val1,val2))
elif operacao == 'Q':
    print('')


#print(soma(val1, val2))
#print(subtracao(val1, val2))
#print(multiplicacao(val1, val2))
#print(divisao(val1, val2))
#print('-' *30)
